﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Draw {
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
            start();
        }

        StackPanel myStackPanel = new StackPanel();

        async void start() {
            await(Task.Delay(20));
            DrawEllipse(5000);
        }

        private void DrawEllipse(int dimension) {
            Ellipse newEllipse = new Ellipse();
            newEllipse.Width = dimension;
            newEllipse.Height = dimension;
            newEllipse.StrokeThickness = 2;
            newEllipse.Stroke = Brushes.Black;
            MainCanvas.Children.Add(newEllipse);
            newEllipse.SetValue(Canvas.LeftProperty, MainCanvas.ActualWidth / 2.0 - dimension / 2.0);
            newEllipse.SetValue(Canvas.TopProperty, MainCanvas.ActualHeight / 2.0 - dimension / 2.0);
            if (dimension > 5) {
                dimension /= 2;
                DrawEllipse(dimension);
            }
        }
    }
}
