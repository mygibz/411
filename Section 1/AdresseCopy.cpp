// AdresseCopy.cpp : Diese Datei enthält die Funktion "main". Hier beginnt und endet die Ausführung des Programms.
//

#include <stdio.h>
#include <string.h>

typedef struct {
	char surname[31];
	char name[31];
	char street[41];
	int postcode;
	char place[31];
} Address;

int main() {
	
	// Gleichzeitige Initialisierung einer Strukturvariablen in ANSI C
	Address person1 = { "Meier", "Hans", "Sternmatt 11a", 6789, "Luzern" };
	// Definition und Zuweisung aller Feldern
	Address person2, teacher;
	strcpy_s(person2.surname, "Meier");
	strcpy_s(person2.name, "Hans");
	strcpy_s(person2.street, "Sternmatt 11a");
	person2.postcode = 6789;
	strcpy_s(person2.place, "Luzern");

	//Bei der Kopie einer Strukturvariablen werden alle Einzelelemente (Felder) der Struktur kopiert:
	teacher = person2;
	printf("teacher> %s, %s, %s, %d\n", teacher.surname, teacher.name, teacher.place, teacher.postcode);
	return 0;
}

