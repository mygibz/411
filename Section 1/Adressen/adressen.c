/* structs.c
 *
 * Copyright 2019 Yanik Ammann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define entrieCount 10

//int strcmp(const char *str1, const char *str2); // for compatibility (not necesairy but removes compile warnings)
//char stcrpy(char *, const char *); // for compatibility (not necesairy but removes compile warnings)


typedef struct {
  char name[31];
  char surname[31];
  char email[127];
} Contact;


// defining global variables
Contact book[entrieCount];
int counter = 1;

// defining functions
void printAdressbuch(Contact book[]);
void addNewEntry(Contact book[], char vorname[], char nachname[], char email[]);
Contact* searchPerson (char nachname[], char vorname[], Contact book[]);


int main (int argc, char *argv[]) {
    Contact tmpContact = {"Kai", "Lee", "info@1yanik3.com"};
    book[0] = tmpContact;

    int option;
	  printf("Simple Adress Book started ...\n");
	  do  {
	    printf("\n\nOptions:\n");
	    printf("Print    .........1\n");
	    printf("Add      .........2\n");
	    printf("Search   .........3\n");
	    printf("Exit     .........0\n");
	    printf("Enter your option>\n");
		  scanf("%d", &option);
		  switch (option) {
			  case 1: //Print
          system("clear");
          printAdressbuch(book);
          break;

			  case 2: //Add new
          system("clear");

          // Vorname
          printf("Add new contact:\nVorname: ");
          char setvorname[31];
          scanf("%s", setvorname);

          // Nachname
          printf("Nachname: ");
          char setnachname[31];
          scanf("%s", setnachname);

          // Email
          printf("Email: ");
          char setemail[127];
          scanf("%s", setemail);

          addNewEntry(book, setvorname, setnachname, setemail);
          break;

			  case 3: //Search
          system("clear");
          // Vorname
          printf("Vorname der gesuchten Person: ");
          char vorname[] = "                         ";
          scanf("%s", vorname);

          // Nachname
          printf("Nachname der gesuchten Person: ");
          char nachname[] = "                         ";
          scanf("%s", nachname);

          // Look for that person
          Contact* foundPerson = searchPerson(vorname, nachname, book);
          if (foundPerson != NULL)
            printf("\nPerson gefunden: %s %s %s\n", foundPerson->name, foundPerson->surname, foundPerson->email);
          else
            printf("\nPerson konnte nicht gefunden werden! \n");
          break;

			  default:
				  option = 0;
			  }
    } while (option != 0);

    return EXIT_SUCCESS;
}






/* ------------------------------------------------------------------------
  Funktion : das Adressbuch auf der Konsole ausgeben

  Kurzbeschreibung:
	Die Funktion gibt alle Einträge der Adressliste auf der Konsole aus.

  Parameter:
  NAME               Datentyp                 Bedeutung
  book               Contact[]                Die Adressliste
------------------------------------------------------------------------*/
void printAdressbuch(Contact book[]) {
  for (int i = 0; i < counter; i++)
    printf("Person %d is %s %s %s\n", i+1, book[i].name, book[i].surname, book[i].email);
}




/* ------------------------------------------------------------------------
  Funktion : fügt einen neuen Eintrag der Adressliste hinzu.

  Kurzbeschreibung:
	Die Funktion fügt einen neuen Eintrag der Adressliste hinzu falls
	die Adressliste noch nicht vollbelegt ist. Die Anzahl Einträge
	wird entsprechend um 1 inkrementiert. Es retorniert true falls
	der neue Eintrag erfolgreich zu der Adressliste hinzugefügt
	werden kann, sonst false.

  Parameter:
  NAME               Datentyp                 Bedeutung
  book               Contact[]                Die Adressliste
  maxlen             int                      Die maximale Anzahl von Noten, die die Adressliste aufnehmen kann
  pNumEntries        int*                     Zeiger auf die Anzahl Kontakte in der Adressliste
  newContact         Contact                  Der neue Kontakt, der zu der Adressliste hinzugefügt werden soll.

  Return:
  true   Falls der neue Eintrag erfolgreich zu der Adressliste hinzugefügt wurde
  false  Sonst
------------------------------------------------------------------------*/
void addNewEntry(Contact book[], char vorname[], char nachname[], char email[]) {
  printf("hey %s %s %s\n", vorname, nachname, email);
  Contact tmpContact;
  strcpy(tmpContact.name, vorname);
  strcpy(tmpContact.surname, nachname);
  strcpy(tmpContact.email, email);
  book[counter] = tmpContact;
  counter++;
}




/* -------------------------------------------------------------------------------
  Funktion : Suche nach einer Person

  Kurzbeschreibung:
	Die Funktion sucht die Person mit name und fname in der Adressliste.
	Es retouniert einen Zeiger auf die erst gefundene Person in der Adressliste
	oder NULL falls die Adressliste leer ist.

  Parameter:
  nachname           char[]                   Name der gesuchten Person
  vorname            char[]                   Vorname der gesuchten Person
  book               Contact[]                Die Adressliste

  Return:
  einen Zeiger auf die gefundene Person in der Adressliste
  NULL falls die Adressliste leer ist.
------------------------------------------------------------------------*/

Contact* searchPerson (char nachname[], char vorname[], Contact book[]) {
  for (int i = 0; i < 3; i++)
      if ((strcmp(vorname, book[i].name) == 0) && (strcmp(nachname, book[i].surname) == 0))
          return &book[i];
    return NULL;
}
