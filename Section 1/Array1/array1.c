/* array1.c
 *
 * Copyright 2019 Yanik Ammann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main (int argc,char *argv[]) {
  char arr[] = "Hello you unfriendly world";
  char arr1[] = "Hello you unfriendly world";
  char arr2[] = "Hello you unfriendly world";
  char arr3[] = "Hello you unfriendly world";
  char arr4[] = "Hello you unfriendly world";
  char arr5[] = "Hello you unfriendly world";
  printf("\"%s\"\n", arr);

  //////////////////////
  // Stringlänge: strlen(x)
  // String-Kopierung: strcpy(x, y)
  // Verketten von Strings: strcat(x, y)
  // Vergleich von Strings: strcmp(x, y)
  //////////////////////


  printf("Length of arr[] according to sizeof(arr): %lu\n", sizeof(arr)); // counts NULL at the end of every "string"
  printf("Length of arr[] according to strlen(arr): %lu\n", strlen(arr));
  printf("Length of arr[] according to strlen(strcat(arr, arr)): %lu\n", strlen(strcat(arr1, arr1)));
  printf("Length of arr[] according to strcpy(arr, arr): %lu\n", strlen(strcpy(arr3, arr4)));
  // count ammount of digits in arr[] with a for loop
  printf("Length of arr[] according to for-loop:");
  int running = 1;
  for (int x = 0; running == 1; x++) {
    if (arr[x] == NULL)
      {
        running = 0;
      }

    // display as table
    if (x % 8 == 0) {
        printf("\n");
      }
    printf("%.2d ", x);
  }
  printf("\n");


  return EXIT_SUCCESS;
}
