/* arrays-2d.c
 *
 * Copyright 2019 Yanik Ammann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <stdlib.h>


#define DIM 17

void drawDiagonalCross() {
  // define and create brett
  int brett[DIM][DIM] = { 0 };
  for (int i = 0; i < DIM; i++)
    {
      for (int j = 0; j < DIM; j++)
        {
          if (i + j == DIM -1 || j == i)
            {
              brett[i][j] = 1;
            }
        }
    }
    // print brett
    for (int i = 0; i < DIM; i++)
      {
        int j;
        for (j = 0; j < DIM; j++)
        {
          printf("%2d", brett[i][j]);
        }
        printf("\n");
      }
}

void drawZurich() {
  // define and create brett
  int brett[DIM][DIM] = { 0 };
  for (int i = 0; i < DIM; i++)
    {
      for (int j = 0; j < DIM; j++)
        {
          if (j <= i)
            {
              brett[i][j] = 1;
            }
        }
    }
    // print brett
    for (int i = 0; i < DIM; i++)
      {
        int j;
        for (j = 0; j < DIM; j++)
        {
          printf("%2d", brett[i][j]);
        }
        printf("\n");
      }
}

int main (int argc, char *argv[]) {
  drawDiagonalCross();
  printf("\n");
  drawZurich();
  return EXIT_SUCCESS;
}
