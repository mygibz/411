/* arrays.c
 *
 * Copyright 2019 Yanik Ammann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <stdlib.h>

float AddToArray(float noten[10]);

int main (int argc, char *argv[]) {
  float noten[10];
  float summe = 0;

  AddToArray(noten);

  // Calculate Average
  for (int i = 0; i < 10; i++) {
    summe += noten[i];
  }

  // output sum and average of grades
  printf("Summe aller noten: %.2f\n", summe);
  printf("Durchschnitt aller noten: %.2f\n", summe / 10);

  return EXIT_SUCCESS;
}


float AddToArray(float noten[10]) {
  for (int i = 0; i < 10; i++) {
    // read grades in
    printf("Note %d eingeben: ", i + 1);
    scanf("%f", &noten[i]);

    // Check if grade is between 1 and 6
    if (noten [i] >= 6 || noten[i] <= 1) {
      printf("FEHLER: Ungültige eingabe\nNote %d eingeben: ", i + 1);
      scanf("%f", &noten[i]);
    }
  }
  return noten[10];
}
