// Sum.cpp : Diese Datei enthält die Funktion "main". Hier beginnt und endet die Ausführung des Programms.
//

/*******************************************************************************
* Programm:
* Filename:   sum.cpp
*             Diese Datei enthält die Implementation von einer einfachen Funktion mit Parameter und Return-Wert.
* Autor:      Xin Cheng
* Version:    1.0
* Datum:
*
* Entwicklungsablauf(Version, Datum, Autor, Entwicklungsschritt, Zeit):
* 1.0   19.09.2018   X.Cheng Erstellung
*
********************************************************************************
*
* Verwendungszweck: C-Schulung, M411
*
* Beschreibung:
*             Diese Datei enthält die Implementation von einer einfachen Funktion mit Parameter und Return-Wert.
*             Hier beginnt und endet die Ausführung des Programms.
*
* Precondition:  -
*
* Postcondition: -
*
* Benötigte Libraries:
* - stdlib.h
* - stdio.h
*
*******************************************************************************/

/***  Include Files: Das ist die vorkompilierte Header-Datei******************/
#include <stdio.h>

/***  Globale Deklarationen und Definitionen **********************************/

int Sum(int a, int b) {
	return a + b;
}


/*******************************************************************************
******************************* HAUPTPROGRAMM **********************************
*******************************************************************************/

int main()
{
	int z1, z2, summe;

	printf("Dieses Programm berechnet die Summe von zwei ganzen Zahlen ...\n");
	printf("Geben Sie zwei ganze Zahlen ein (getrennt mit Leerstelle)>");
	scanf_s("%d %d", &z1, &z2);
	summe = Sum(z1, z2);
	printf("Die Summe von %d und %d ist %d\n", z1, z2, summe);

	return 0;
}




