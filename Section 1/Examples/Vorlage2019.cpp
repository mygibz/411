/*******************************************************************************
* Programm:
* Filename:   Vorlage2019.c
*
* Autor:
* Version:    1.0
* Datum:
*
* Entwicklungsablauf(Version, Datum, Autor, Entwicklungsschritt, Zeit):
* 1.0   07.08.2019   X.Cheng Erstellung
*
********************************************************************************
*
* Verwendungszweck: C-Schulung, M411
*
* Beschreibung:
*             Diese Programm dient als Vorlage für die Dokumentation.
*             Diese Datei enthält die Funktion "main". Hier beginnt und endet die Ausführung des Programms.
*
* Precondition:  -
*
* Postcondition: -
*
* Benötigte Libraries:
* - stdlib.h
* - stdio.h
*
*******************************************************************************/

/***  Include Files ******************/
#include "stdio.h"

/***  Globale Deklarationen und Definitionen **********************************/


/***  Funktions-Deklarationen *************************************************/



/*******************************************************************************
******************************* HAUPTPROGRAMM **********************************
*******************************************************************************/

int main()
{
	// loakle Variablen                                                                  


  /* Intro --------------------- */


  /* Eingabe ------------------- */


  /* Verarbeitung -------------- */


  /* Ausgabe ------------------- */
	printf("Hello World!\n");

	return 0;
}

/*******************************************************************************
*************************** FUNKTIONS-DEFINITIONEN *****************************
*******************************************************************************/

/*******************************************************************************
* Funktion:
* Source: 	  .c
* Call:
*
* Autor:
* Version:   1.0
* Datum:
*
* Entwicklungsablauf(Version, Datum, Autor, Entwicklungsschritt):
* 1.0
********************************************************************************
*
* Verwendungszweck: C-Schulung, M411
*
* Beschreibung:
*
*
* Precondition: -
*
* Postcondition: -
*
* Benötigte Unterprogramme: -
*
* Parameter: (I: Input, O: Output, U: Update)
*
*
* Copyright (c) 201x by
*******************************************************************************/
