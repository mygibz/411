/*******************************************************************************
* Programm:
* Filename:   fktNoParam.cpp
*             Diese Datei enthält die Implementation von einer einfachen Funktion ohne Parameter und das Testprogramm.
* Autor:      Xin Cheng
* Version:    1.0
* Datum:
*
* Entwicklungsablauf(Version, Datum, Autor, Entwicklungsschritt, Zeit):
* 1.0   19.09.2018   X.Cheng Erstellung
*
********************************************************************************
*
* Verwendungszweck: C-Schulung, M411
*
* Beschreibung:
*             Diese Datei enthält die Implementation von einer einfachen Funktion ohne Parameter und die Funktion "main".
*            Hier beginnt und endet die Ausführung des Programms.
*
* Precondition:  -
*
* Postcondition: -
*
* Benötigte Libraries:
* - stdlib.h
* - stdio.h
*
*******************************************************************************/

/***  Include Files: Das ist die vorkompilierte Header-Datei******************/
#include "stdio.h"

/***  Globale Deklarationen und Definitionen **********************************/

/*******************************************************************************
* Funktion:  hallo
*
* Beschreibung:
*    This function outputs a string: "hallo".
*
* Parameter: (I: Input, O: Output, U: Update)
*
*******************************************************************************/

void hallo() {
	printf("In der Funktion\n");
}

/***  Funktions-Deklarationen *************************************************/

/*******************************************************************************
******************************* HAUPTPROGRAMM **********************************
*******************************************************************************/

int main()
{
	printf("Vor der Funktion\n");
	hallo();
	printf("Nach der Funktion\n");
	return 0;
}