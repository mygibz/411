// RepZeigerMani.cpp : Diese Datei enthält die Funktion "main". Hier beginnt und endet die Ausführung des Programms.
//

#include <stdio.h>

int main()
{
	int number = 0x56CAC012;
	unsigned char* chPtr = (unsigned char*)& number;    // "unsigned char" weil char = 1byte und 
	unsigned char** chPPtr = &chPtr;                    // int = 4 byte (2 hex-ziffern vs 8 hex-ziffern)

	printf("chPtr = %p, *chPtr = %X, **chPPtr = %X\n\n", chPtr, *chPtr, **chPPtr);

	chPtr++;

	printf("chPtr = %p, *chPtr = %X, **chPPtr = %X\n\n", chPtr, *chPtr, **chPPtr);

	return 0;
}
