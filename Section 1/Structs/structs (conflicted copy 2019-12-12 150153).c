/* structs.c
 *
 * Copyright 2019 Yanik Ammann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <stdlib.h>

typedef struct {
  char name[31];
  char surname[31];
  int alter;
} Person;

int main (int argc, char *argv[])
{
  Person pp = { "Toni", "Klein", 44};

  Person p1 = { "Otto", "Mein", 77 };
  Person p2 = { "Otto", "Meier",  89 };

  p2 = p1;

  printf ("pp is %s %s %d\n", pp.name, pp.surname, pp.alter);
  printf ("p1 is %s %s %d\n", p1.name, p1.surname, p1.alter);
  printf ("p2 is %s %s %d\n", p2.name, p2.surname, p2.alter);
  return EXIT_SUCCESS;
}
