/* structs.c
 *
 * Copyright 2019 Yanik Ammann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <stdlib.h>

typedef struct {
  char name[31];
  char surname[31];
  int alter;
} Person;

int
main (int argc, char *argv[])
{
  Person pp = { "Toni", "Klein" };

  Person p1 = { "Otto", "Meier"};
  Person p2 = { "Otto", "Meier"};

  printf ("pp is %s %s\n", pp.name, pp.surname);
  printf ("p1 is %s %s\n", p1.name, p1.surname);
  printf ("p2 is %s %s\n", p2.name, p2.surname);
  return EXIT_SUCCESS;
}
