/* structs.c
 *
 * Copyright 2019 Yanik Ammann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <stdlib.h>

int strcmp(const char *str1, const char *str2); // for compatibility

typedef struct {
  char name[31];
  char surname[31];
  int alter;
} Person;

Person* searchPerson (Person personen[], char vorname[], char nachname[]) {
  for (int i = 0; i < 3; i++)
      if ((strcmp(vorname, personen[i].name) == 0) && (strcmp(nachname, personen[i].surname) == 0))
          return &personen[i];
    return NULL;
  }

int main (int argc, char *argv[])
{
  Person personen[3] = { {"Kai", "Lee", 18}, {"Bailey", "Carr", 16}, {"Ollie", "Merrit", 16} };
  personen[2] = personen[0];

  // search
  Person* foundPerson = searchPerson(personen, "Kai", "Lee");
  printf ("Person is %s %s %d\n", foundPerson->name, foundPerson->surname, foundPerson->alter);
  return EXIT_SUCCESS;
}
