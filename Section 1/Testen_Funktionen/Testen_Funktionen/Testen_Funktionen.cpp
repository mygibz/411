/*******************************************************************************
* Programm:   Testen_Funktionen
* Filename:   Testen_Funktionen.cpp
*
* Autor:      Yanik Ammann
* Version:    1.0
* Datum:	  03.10.2019
*
* Entwicklungsablauf(Version, Datum, Autor, Entwicklungsschritt, Zeit):
* 1.0, 03.10.2019, Yanik Ammann, Entwicklungsstart
*
********************************************************************************
*
* Verwendungszweck: C-Schulung, M411
*
* Beschreibung:
*             Verwendet Funktionen und gibt die grésste Zahl, deren letzte Ziffer und die Queersumme aus.
*
* Precondition:  
*	-
*
* Postcondition:
*	-
*
* Benötigte Libraries:
* - iostream.h
* - stdio.h
*
*******************************************************************************/

/***  Include Files ******************/
#include <stdio.h>
#include <iostream>

/***  Globale Deklarationen und Definitionen **********************************/
int GetMaxValue(int a, int b);
int CalcQuersumme(int a);
int GetZiffer(int a, int b);

/****************************** HAUPTPROGRAMM *********************************/

int main()
{
	// loakle Variablen
	int a;
	int b;

	/* Eingabe ------------------- */
	printf_s("Gebe Zahl 1 ein: ");
	scanf_s("%d", &a);

	printf_s("Gebe Zahl 2 ein: ");
	scanf_s("%d", &b);

	/* Ausgabe ------------------- */
	int zahl = GetMaxValue(a, b);
	printf_s("\nGroessere Zahl: %d\n", zahl);
	printf_s("Die %d-the Ziffer davon: %d\n", b, GetZiffer(zahl, b));
	printf_s("Die Queersumme von %d: %d\n", GetMaxValue(a, b), CalcQuersumme(zahl));

	return 0;
}


/***  Funktions-Deklarationen *************************************************/

int GetMaxValue(int a, int b) {
	int out;
	if (a > b) {
		out = a;
	}
	else {
		out = b;
	}
	return out;
}

int CalcQuersumme(int a) {
	int first = a;
	int out = 0;

	while (a > 0) {
		out = out + a % 10;
		a = a / 10;
	}
	return out;
}

int GetZiffer(int a, int b) {
	for (int i = 0; i < b; i++) {
		a = a / 10;
	}
	return a % 10;
}



