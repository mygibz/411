/*******************************************************************************
* Programm:
* Filename:   StringDef.cpp
*
* Autor:      Xin Cheng
* Version:    1.0
* Datum:
*
* Entwicklungsablauf(Version, Datum, Autor, Entwicklungsschritt, Zeit):
* 1.0   19.09.2018   X.Cheng Erstellung
*
********************************************************************************
*
* Verwendungszweck: C-Schulung, M411
*
* Beschreibung:
*             Diese Datei enthält ein Beispielprogramm, welches eine Zeichenkette
*             ein Array definiert und initialisiert.
*
* Precondition:  -
*
* Postcondition: -
*
* Benötigte Libraries:
* - stdlib.h
* - stdio.h
*
*******************************************************************************/

/***  Include Files: Das ist die vorkompilierte Header-Datei******************/
#include "stdio.h"

/***  Globale Deklarationen und Definitionen **********************************/



/*******************************************************************************
******************************* HAUPTPROGRAMM **********************************
*******************************************************************************/

int main()
{
	//Folgende zwei Definitionen sind gleichwertig.
	char string1[20] = "Hallo";  //Das ist die bevorzugte Schreibweise
	char string2[] = "Hallo";
	char string3[20] = { 'H', 'a', 'l', 'l', 'o','\0' };

	printf("%s\n", string1);  //Ausgabe einer Zeichkette mit %s
	printf("%s\n", string2);
	printf("%s\n", string3);

	return 0;
}
