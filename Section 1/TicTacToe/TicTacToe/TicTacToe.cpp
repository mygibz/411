/*******************************************************************************
* Programm:   Programm zur Implementierung des Spiels TicTacToe
* Filename:   TicTacToe_V1.cpp
*
* Autor:      Xin Cheng
* Version:    1.0
* Datum:      12.11.2018
*
* Entwicklungsablauf(Version, Datum, Autor, Entwicklungsschritt):
* 1.0   12.11.2018   X. Cheng   Ganzes Programm erstellt
********************************************************************************
*
* Verwendungszweck: M411
*
* Beschreibung:
* Tic-Tac-Toe oder Drei gewinnt (auch Kreis und Kreuz, Dodelschach) ist ein klassisches, einfaches
* Zweipersonen-Strategiespiel. Auf einem quadratischen, 3×3 Felder großen Spielfeld setzen die beiden
* Spieler abwechselnd ihr Zeichen (ein Spieler Kreuze, der andere Kreise) in ein freies Feld. Der Spieler,
* der als Erster drei Zeichen in eine Zeile, Spalte oder Diagonale setzen kann, gewinnt. Wenn allerdings
* beide Spieler optimal spielen, kann keiner gewinnen, und es kommt zu einem Unentschieden. Das heißt,
* alle neun Felder sind gefüllt, ohne dass ein Spieler die erforderlichen Zeichen in einer Reihe, Spalte
* oder Diagonalen setzen konnte.
*
* Dieses Programm implementiert eine stark vereinfachte Version des Spiels. In diesem Programm spielt der Benutzer
* alleine. Alle Felder sind mit der Ziffer 0 bis 8 nummeriert. Der Benutzer wird wiederholt aufgefordert
* die Position seines nächsten Feldes einzugeben, bis eine Reihe voll belegt wird.
*
* Kernfunktionalität dieses Programms ist die Funktion checkRow(), welche ausgehend vom letzten belegten Feld überprüft,
* ob eine diagonale, horizontale oder vertikale Reihe voll belegt ist.
*
* Precondition:  -
*
* Postcondition: -
*
* Benötigte Libraries:
* - stdio.h
*
* Copyright (c) 2018 by X.Cheng
*******************************************************************************/

#include <stdio.h>

#define dim 3

void printPlayField(int field[][dim]);
bool checkRow(int field[][dim], int userID);

int newSelected;
int newSelectedRow;
int newSelectedColumn;


int main() {
	/*** Local variables ****/
	//The play field, defined as a 2d array. All elements will be initialised to 0.
	int playField[dim][dim] = { 0 };
	int posIn;                               //input position: 0--8

	/* Intro --------------------- */
	printf("TicTacToe started...\n");
	printPlayField(playField);

	int userID = 1;
	while (true) {
		// input
		printf("\n\n Player%d: Enter the field you want to select (1-9): ", userID);
		scanf_s("%d", &newSelected); 
		// check if already written (no overwrites)
		if (playField[(newSelected - 1) / dim][(newSelected - 1) % dim] == 0) {
			playField[(newSelected - 1) / dim][(newSelected - 1) % dim] = userID;
		} else {
			// reset
			printf("\n Field is already in use!\n");
			userID = userID % 2 + 1;
		}

		// display field
		printPlayField(playField);

		// check if won
		if (checkRow(playField, userID) == true) {
			printf("GAME OVER! PLAYER %d WON\n", userID);
			break;
		} else {
			userID = userID % 2 + 1;
		}

	}

	return 0;
}

// prints playfield
void printPlayField(int field[][dim]) {
	printf("------------\n");
	char occupationChar[3] = { ' ', 'O', 'X' };
	for (int i = 0; i < dim; i++) { //row
		for (int j = 0; j < dim; j++) { //column
			printf("%2c |", occupationChar[field[i][j]]);
		}
		printf("\n------------\n");
	}
}


// checks if math is won (checks row, column and diagonals)
bool checkRow(int field[][dim], int userID) {
	bool found = false;
	
	// rows
	for (int i = 0; i < dim; i++) { //row
		int foundInRow = 0;
		for (int j = 0; j < dim; j++) { //column
			if (field[i][j] == userID) {
				foundInRow++;
			}
		}
		if (foundInRow == dim) {
			found = true;
		}
	}

	// columns
	for (int i = 0; i < dim; i++) { //column
		int foundInColumn = 0;
		for (int j = 0; j < dim; j++) { //row
			if (field[j][i] == userID) {
				foundInColumn++;
			}
		}
		if (foundInColumn == dim) {
			found = true;
		}
	}

	// from top left to bottom right
	int foundInDiagonal = 0;
	for (int i = 0; i < dim; i++) { //column
		for (int j = 0; j < dim; j++) { //row
			if (field[j][i] == userID && i == j) {
				foundInDiagonal++;
			}
		}
		if (foundInDiagonal == 3) {
			found = true;
		}
	}
	// from top right to bottom left
	foundInDiagonal = 0;
	for (int i = 0; i < dim; i++) { //column
		for (int j = 0; j < dim; j++) { //row
			if (field[j][i] == userID && i + j == dim -1) {
				foundInDiagonal++;
			}
		}
		if (foundInDiagonal == dim) {
			found = true;
		}
	}


	return found;
}






