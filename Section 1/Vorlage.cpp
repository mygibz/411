#include <stdio.h>
#define MAX_LEN   21
#define MAX_NUM_PERSONEN 100

//Definition der Datentyp Schulnote:





typedef enum { Exit, Print, Average, Best, Add }
Options;
void printMenu();
void PrintNotenliste(Schulnote list[], int numEntries);
float CalcAverage(Schulnote list[], int numEntries);
Schulnote* FindBest(Schulnote list[], int numEntries);
int AddNewEntry(Schulnote list[], int maxlen, int *pNumEntries, Schulnote newNote);


int main()
{
	//Your Code:

	Options op;
	

	//Fügen Sie hier eine Schleife. Die Benutzereingabe und die anschliessende Switch-Anweisung 
	//sollen solange wiederholt werden bis der Benutzer 0 (Exit) eingibt.

		printMenu();
		scanf_s("%d", &op);

		switch (op) {
		case Print:
			//Your Code:
			break;
		case Average:
			//Your Code:
			break;
		case Best:
			//Your Code:
			break;
		case Add:
			//Your Code:
			break;

		case Exit:
			//Your Code:
			break;
		default:
			//Your Code:
			break;
		}
	

	return 0;
}

void printMenu() {
	printf("Options:\n");
	printf("Print           .........%d\n", Print);
	printf("Average         .........%d\n", Average);
	printf("Find Best       .........%d\n", Best);
	printf("Add             .........%d\n", Add);
	printf("Exit            .........%d\n", Exit);
	printf("Enter your option>\n");
}

/* ------------------------------------------------------------------------
  Funktion : die Notenliste auf der Konsole ausgeben

  Kurzbeschreibung:
	Die Funktion gibt alle Einträge der Notenliste auf der Konsole aus.

  Parameter:
  NAME               Datentyp                 Bedeutung
  list               Schulnote[]              Die Notenliste
  numEntries         int                      Anzahl Einträge in der Notenliste
------------------------------------------------------------------------*/
void PrintNotenliste(Schulnote list[], int numEntries) {
	printf("Alle Noten:\n");
	for (int i = 0; i < numEntries; i++) {
		printf("Name: %s, Vorname: %s, Note: %f\n", list[i].name, list[i].vorname, list[i].note);
	}

}

/* ------------------------------------------------------------------------
  Funktion : berechnet den Durchschnitt aller Noten in der Notenliste

  Kurzbeschreibung:
	Die Funktion retorniert den Durchschnitt aller Noten in der Notenliste.

  Parameter:
  NAME               Datentyp                 Bedeutung
  list               Schulnote[]              Die Notenliste
  numEntries         int                      Anzahl Einträge in der Notenliste

  Return:
  Den Durchschnitt aller Noten in der Notenliste
------------------------------------------------------------------------*/
float CalcAverage(Schulnote list[], int numEntries) {
	//Your code

	return 0;
}

/* -------------------------------------------------------------------------------
  Funktion : Suche nach der besten Schulnote

  Kurzbeschreibung:
	Die Funktion sucht die beste Schulnote in der Notenliste.
	Es retouniert einen Zeiger auf die beste Schulnote in der Notenliste
	oder NULL falls die Notenliste leer ist.

  Parameter:
  NAME               Datentyp                 Bedeutung
  list               Schulnote[]              Die Notenliste
  numEntries         int                      Anzahl Einträge in der Notenliste

  Return:
  einen Zeiger auf die beste Schulnote in der Notenliste
  NULL falls die Notenliste leer ist.
------------------------------------------------------------------------*/
Schulnote* FindBest(Schulnote list[], int numEntries) {
	//Your code

	return NULL;
}

/* ------------------------------------------------------------------------
  Funktion : fügt einen neuen Eintrag der Notenliste hinzu.

  Kurzbeschreibung:
	Die Funktion fügt einen neuen Eintrag der Notenliste hinzu falls
	die Notenliste noch nicht vollbelegt ist. Die Anzahl Noten in der Notenliste
	wird entsprechend um 1 inkrementiert. Es retorniert true falls
	der neue Eintrag erfolgreich zu der Notenliste hinzugefügt
	werden kann, sonst false.

  Parameter:
  NAME               Datentyp                 Bedeutung
  list               Schulnote[]              Die Notenliste
  maxlen             int                      Die maximale Anzahl von Noten, die die Notenliste aufnehmen kann
  pNumEntries        int*                     Zeiger auf Anzahl Noten in der Notenliste
  newNote            Schulnote                Die neue Note, die zu der Notenliste hinzugefügt werden soll.

  Return:
  1  (true)   Falls der neue Eintrag erfolgreich zu der Notenliste hinzugefügt wurde
  0  (false)  Sonst
------------------------------------------------------------------------*/
int AddNewEntry(Schulnote list[], int maxlen, int *pNumEntries, Schulnote newNote) {
	//Your Code
	return 0;
}

