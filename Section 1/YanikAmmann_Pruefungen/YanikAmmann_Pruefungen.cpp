#include <stdio.h>
#include <string.h>
#define MAX_LEN   21
#define MAX_NUM_PERSONEN 100

//Definition der Datentyp Schulnote:
typedef struct {
	char name[20];
	char vorname[20];
	float note;
} Schulnote;




typedef enum { Exit, Print, Average, Best, Add }
Options;
void printMenu();
void PrintNotenliste(Schulnote list[], int numEntries);
float CalcAverage(Schulnote list[], int numEntries);
Schulnote FindBest(Schulnote list[], int numEntries);
int AddNewEntry(Schulnote list[], int maxlen, int pNumEntries, Schulnote newNote);


int main() {
	//Your Code:

	Options op;
	int itemsInList = 2;
	Schulnote notenliste[10]{ {"Max", "Muster", 6}, {"Hans", "Tester", 5}, {"Hello", "World", 4} };
	int running = 1;

	//Fügen Sie hier eine Schleife. Die Benutzereingabe und die anschliessende Switch-Anweisung 
	//sollen solange wiederholt werden bis der Benutzer 0 (Exit) eingibt.


	do {
		printMenu();
		scanf_s("%d", &op);

		switch (op) {
		case 1:
			PrintNotenliste(notenliste, itemsInList);
			break;
		case 2:
			printf("\nDurchschnitt aller Schulnoten: %f", CalcAverage(notenliste, itemsInList));
			break;
		case 3:
			Schulnote bestNote = FindBest(notenliste, itemsInList);
			printf("\nDie Bestnote %f wurde von %s %s geschrieben.\n", bestNote.note, bestNote.vorname, bestNote.name);
			break;
		case 4:
			Schulnote tmpNote;

			printf("Neue Schulnote Erstellen:\nVorname:\n");
			scanf_s("%s", tmpNote.vorname, sizeof(notenliste[itemsInList].vorname));
			printf("Nachname:\n");
			scanf_s("%s", tmpNote.name, sizeof(notenliste[itemsInList].name));
			printf("Note:\n");
			scanf_s("%f", &tmpNote.note);

			AddNewEntry(notenliste, 10, itemsInList, tmpNote);
			itemsInList++;
			break;
		case 0:
			running = 0;
			break;
		default:
			printf("Falsche Eingabe, bitte erneut versuchen.");
			break;
		}
	} while (running == 1);

	


	return 0;
}

void printMenu() {
	printf("\n\nOptions:\n");
	printf("Print           .........%d\n", Print);
	printf("Average         .........%d\n", Average);
	printf("Find Best       .........%d\n", Best);
	printf("Add             .........%d\n", Add);
	printf("Exit            .........%d\n", Exit);
	printf("Enter your option>\n");
}

/* ------------------------------------------------------------------------
  Funktion : die Notenliste auf der Konsole ausgeben

  Kurzbeschreibung:
	Die Funktion gibt alle Einträge der Notenliste auf der Konsole aus.

  Parameter:
  NAME               Datentyp                 Bedeutung
  list               Schulnote[]              Die Notenliste
  numEntries         int                      Anzahl Einträge in der Notenliste
------------------------------------------------------------------------*/
void PrintNotenliste(Schulnote list[], int numEntries) {
	printf("Alle Noten:\n");
	for (int i = 0; i <= numEntries; i++) {
		printf("%d Name: %s, Vorname: %s, Note: %f\n", i, list[i].name, list[i].vorname, list[i].note);
	}
}

/* ------------------------------------------------------------------------
  Funktion : berechnet den Durchschnitt aller Noten in der Notenliste

  Kurzbeschreibung:
	Die Funktion retorniert den Durchschnitt aller Noten in der Notenliste.

  Parameter:
  NAME               Datentyp                 Bedeutung
  list               Schulnote[]              Die Notenliste
  numEntries         int                      Anzahl Einträge in der Notenliste

  Return:
  Den Durchschnitt aller Noten in der Notenliste
------------------------------------------------------------------------*/
float CalcAverage(Schulnote list[], int itemsInList) {
	float durchschitsSumme = 0;
	for (int i = 0; i <= itemsInList; i++) {
		durchschitsSumme += list[i].note;
	}
	return durchschitsSumme / (itemsInList + 1);
}

/* -------------------------------------------------------------------------------
  Funktion : Suche nach der besten Schulnote

  Kurzbeschreibung:
	Die Funktion sucht die beste Schulnote in der Notenliste.
	Es retouniert einen Zeiger auf die beste Schulnote in der Notenliste
	oder NULL falls die Notenliste leer ist.

  Parameter:
  NAME               Datentyp                 Bedeutung
  list               Schulnote[]              Die Notenliste
  numEntries         int                      Anzahl Einträge in der Notenliste

  Return:
  einen Zeiger auf die beste Schulnote in der Notenliste
  NULL falls die Notenliste leer ist.
------------------------------------------------------------------------*/
Schulnote FindBest(Schulnote list[], int numEntries) {
	int bestnote = 0;
	for (int i = 0; i < numEntries; i++) {
		if (list[bestnote].note < list[i].note) {
			bestnote = i;
		}
	}
	return list[bestnote];
}

/* ------------------------------------------------------------------------
  Funktion : fügt einen neuen Eintrag der Notenliste hinzu.

  Kurzbeschreibung:
	Die Funktion fügt einen neuen Eintrag der Notenliste hinzu falls
	die Notenliste noch nicht vollbelegt ist. Die Anzahl Noten in der Notenliste
	wird entsprechend um 1 inkrementiert. Es retorniert true falls
	der neue Eintrag erfolgreich zu der Notenliste hinzugefügt
	werden kann, sonst false.

  Parameter:
  NAME               Datentyp                 Bedeutung
  list               Schulnote[]              Die Notenliste
  maxlen             int                      Die maximale Anzahl von Noten, die die Notenliste aufnehmen kann
  pNumEntries        int*                     Zeiger auf Anzahl Noten in der Notenliste
  newNote            Schulnote                Die neue Note, die zu der Notenliste hinzugefügt werden soll.

  Return:
  1  (true)   Falls der neue Eintrag erfolgreich zu der Notenliste hinzugefügt wurde
  0  (false)  Sonst
------------------------------------------------------------------------*/
int AddNewEntry(Schulnote list[], int maxlen, int pNumEntries, Schulnote newNote) {
	pNumEntries++;

	if (pNumEntries >= maxlen)
		return 0;

	printf("%d %s %s", pNumEntries,newNote.vorname, newNote.name);

	strcpy_s(list[pNumEntries].name, newNote.name);
	strcpy_s(list[pNumEntries].vorname, newNote.vorname);
	list[pNumEntries].note = newNote.note;

	//list[*pNumEntries] = newNote;

	return 1;
}

