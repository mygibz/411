/* zeigermanipulationen.c
 *
 * Copyright 2019 Yanik Ammann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <stdlib.h>

int
main (int argc,
      char *argv[])
{
  int number = 0xFFFEDCBA;   //hexadezimalzahl
  int *ptr = &number;
  int **pptr = &ptr;
  printf("number = %X; *ptr= %X; **ptr= %X\n", number, *ptr, **pptr);

  int myInt = 2;
  unsigned char *pch = &myInt;
  printf("%X ist %ld gross und hat eine Zeigergrösse von %ld\n", *pch, sizeof(myInt), sizeof(*pch));
  return EXIT_SUCCESS;
}
