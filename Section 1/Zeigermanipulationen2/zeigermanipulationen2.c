/* zeigermanipulationen2.c
 *
 * Copyright 2019 Yanik Ammann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <stdlib.h>

int
main (int argc,
      char *argv[])
{
  int ZahlA = 3, ZahlB = 5;
  int *PtrA = &ZahlA, *PtrB = &ZahlB, *PtrC = PtrA;
  printf("ZahlA = %u, ZahlB = %u, *PtrA = %u, *PtrB = %u, *PtrC = %u \n",
  ZahlA, ZahlB, *PtrA, *PtrB, *PtrC );
  printf("Ausgabe 1 : %u \n", PtrA == &ZahlA );
  printf("Ausgabe 2 : %u \n", * * & PtrA );
  printf("Ausgabe 3 : %u \n", 7 * *PtrA / *PtrB + 7 );
  printf("Ausgabe 4 : %u \n", *((char *)PtrB + 1) );
  printf("Ausgabe 5 : %u \n", *(PtrC = &ZahlB) *= *PtrA + 1 );
  printf("ZahlA = %u, ZahlB = %u, *PtrA = %u, *PtrB = %u, *PtrC = %u \n",
  ZahlA, ZahlB, *PtrA, *PtrB, *PtrC );
  return EXIT_SUCCESS;
}
