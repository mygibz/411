int Intcmp(int val1, int val2);
int main(void) {
	int ival1, ival2, cmp;
	printf("Wert 1 eingeben: ");
	scanf_s("%d", &ival1);
	printf("Wert 2 eingeben: ");
	scanf_s("%d", &ival2);

	//Hier soll die Funktion Intcmp() mit Argumente ival1, ival2 aufgerufen werden.
	cmp = Intcpm(ival1, ival2);
	;
	switch (cmp){
		case 1:
		printf("%d ist der groessere Wert.\n",ival1);
		break;
		case -1:
		printf("%d ist der groessere Wert.\n",ival2);
		break;
		default:
		printf("Beide Werte sind gleich!\n");
		break;
	}
	return 0;
}
/*Diese Funktion vergleicht den Parameter val1 und val2 und retourniert:
1 falls val1 > val2
0 falls val1 == val2
-1 falls val1 < val2*/
int Intcmp(int val1, int val2) {
	if (val1 > val2) {
		return 1;
	} else {
		if (val1 == val2) {
			return 0;
		} else {
			return -1;
		}
	}
}
