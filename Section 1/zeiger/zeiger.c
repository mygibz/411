/* zeiger.c
 *
 * Copyright 2019 Yanik Ammann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <stdlib.h>


void Swap(int* p1, int* p2) {
  int tmp = *p1;;
  *p1 = *p2;
  *p1 = tmp;
}



int main (int argc, char *argv[]) {
  int *pa;
  int *pb;
  int a = 63;
  int b = 127;

  pa = &a;
  pb = &b;

  printf ("Value of Variable a = %d\n", a);
  printf ("Value of Variable b = %d\n", b);
  printf ("Value of Pointer pa = %d\n", pa);
  printf ("Value of Pointer &pb = %d\n\n", &pb);

  printf ("Location of Variable a = %p\n", a);
  printf ("Location of Variable &b = %p\n", &b);
  printf ("Location of Pointer pa = %p\n", pa);
  printf ("Location of Pointer &pb = %p\n\n", &pb);

  a = 127;
  b = 63;

  printf ("Location of changend Variable a = %p\n", a);
  printf ("Location of changend Variable &b = %p\n", &b);
  printf ("Location of Pointer pa = %p\n", pa);
  printf ("Location of Pointer &pb = %p\n\n", &pb);

  Swap(pa, pb);

  printf ("\nValue of changed Variable a = %d\n", a);
  printf ("Value of changed Variable b = %d\n", b);
  printf ("Value of changed Pointer pa = %d\n", pa);
  printf ("Value of changed Pointer &pb = %d\n\n", &pb);

  printf ("Location of changend Variable a = %p\n", a);
  printf ("Location of changend Variable &b = %p\n", &b);
  printf ("Location of Pointer pa = %p\n", pa);
  printf ("Location of Pointer &pb = %p\n\n", &pb);

  return EXIT_SUCCESS;
}



