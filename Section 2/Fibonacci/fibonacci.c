/* fibonacci.c
 *
 * Copyright 2020 Yanik Ammann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <stdlib.h>

void iterative(int runs) {
  int x = 0;
  int y = 1;
  for (int i = 0; i < runs; i++)
    {
        printf("%d ", x);
        int tmp = 0;
        tmp = x + y;
        y = x;
        x = tmp;
    }
}

int recursive(int n) {
   if (n == 0 || n == 1)
      return n;
   else
      return (recursive(n-1) + recursive(n-2));
}


int main (int argc, char *argv[]) {
  printf ("\niterative:\n");
  iterative(11);
  printf ("\nrecursive:\n");
  for(int i = 0;i<11;i++) {
      printf("%d ",recursive(i));
   }
  return EXIT_SUCCESS;
}
