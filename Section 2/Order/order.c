/* order.c
 *
 * Copyright 2020 Yanik Ammann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <stdlib.h>

/*

Bubble sort

void swap(int *xp, int *yp)
{
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}

// A function to implement bubble sort
void bubbleSort(int arr[], int n)
{
    int i, j;
    for (i = 0; i < n-1; i++)

    // Last i elements are already in place
    for (j = 0; j < n-i-1; j++)
        if (arr[j] > arr[j+1])
            swap(&arr[j], &arr[j+1]);
}

// Function to print an array
void printArray(int arr[], int size)
{
    int i;
    for (i = 0; i < size; i++)
        printf("%d ", arr[i]);
    printf("\n");
}

// Driver code
int
main (int argc,
      char *argv[])
{
    int arr[] = {64, 34, 25, 12, 22, 11, 90};
    int n = sizeof(arr)/sizeof(arr[0]);
    bubbleSort(arr, n);
    printf("Sorted array: \n");
    printArray(arr, n);
  return EXIT_SUCCESS;
}*/



/*
 * File:   StraightInsertionSort.c
 * Author: https://www.roytuts.com
 */

#include <stdio.h>
#include <stdlib.h>

void straightInsertionSort(int a[], int n) {
    int i = 1, j, k = 0;
    int temp;
    while (i < n) {
        j = i - 1;
        temp = a[j + 1];
        k = i;
        while (j >= 0) {
            if (temp < a[j]) {
                k = j;
                j--;
            } else break;
        }
        j = i;
        while (j > k) {
            a[j] = a[j - 1];
            j--;
        }
        a[j] = temp;
        i++;
    }
    printf("\n");
    printf("\nThe sorted array elements are given below\n");
    for (i = 0; i < n; i++) {
        printf("a[%d]=%d ", i, a[i]);
    }
}

int main (int argc, char *argv[]) {
  int i, n = 6;
  int a[] = {15, 8, 17, 12, 38, 19};
  printf("\n:: Straight Insertion Sort ::\n");
  printf("\nInput array elements\n");
  for (i = 0; i < n; i++)
      printf("a[%d]=%d ", i, a[i]);
  straightInsertionSort(a, n);
  printf("\n\n");
  return EXIT_SUCCESS;
}
