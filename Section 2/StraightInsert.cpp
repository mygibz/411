/**
   Quelle: https://www.zentut.com/c-tutorial/insertion-sort-in-c/

   Introduction to insertion sort algorithm
Insertion sort in CIn the insertion sort algorithm, we sort a unsorted array 
by inserting its elements to the proper positions in a sorted array. The 
insertion sort algorithm is efficient in case the array is substantially sorted.
It is more efficient to use the quicksort or heapsort algorithm to sort a big 
list of unsorted elements.

When we sort something manually, we often use the insertion sort technique 
e.g., when we sort a deck of playing cards. We can describe the insertion sort 
algorithm similar to sort a deck of playing cards:

First, we start with an empty set of cards (on our hand i.e., the sorted array) 
and the cards on the table i.e., the unsorted array.
Then we pick one card at a time from the table i.e., unsorted array,  and insert
it into the proper position in our hand  i.e., the sorted array
In order to insert a card in the correct position, we compare it with the 
existing cards in our hand from right to left.
Notice that the cards that we hold are always sorted.

The complexity of the insertion sort algorithm is О(n2) where n is the number 
of elements that need to be sorted.
///////////rm a.out;gcc StraightInsert.cpp; ./a.out;echo
**/
#include <stdio.h>

#define SIZE 10
void insertion_sort(int a[], const int size);
int main()
{
	int a[SIZE] = { 3, 4 , 6, 1, 5, 8, 7, 9, 0, 2 };

	insertion_sort(a, SIZE);

	for (int i = 0; i < SIZE; i++)
	{
		printf("%d ", a[i]);
	}

	return 0;
}

void insertion_sort(int a[], const int size)
{

	int i, j, k;
	for (i = 1; i < size; ++i)          // We iterate through the unsorted cards,
	                                    // beginning from the second card.
	{
		k = a[i];                       // k holds the value of the current card to be inserted into the sorted part.
		j = i - 1;                      // start position for comparision: the card left to the current card
		while ((j >= 0) && (k < a[j]))  // find the right position for insertion by comparing the currrent card
		                                // with the cards of the sorted part beginning from the card left to the current card.
		{                               
			a[j + 1] = a[j];            //shift a[j] to the right by one
			j--;                        //take the next card on the left
		}
		a[j + 1] = k;	                //correction of index
	}
}
